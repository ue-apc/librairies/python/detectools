from detectools.train.trainer import Trainer
from detectools.train.loss_aggregator import Aggregator

__all__ = ("Trainer", "Aggregator")
