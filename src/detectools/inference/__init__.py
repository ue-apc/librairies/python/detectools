from detectools.inference.predictor import Predictor, InferenceImage

__all__ = ("Predictor", "InferenceImage")