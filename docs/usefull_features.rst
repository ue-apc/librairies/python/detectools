Usefull features
====================

Here are some usefull features for data management and data visualisation.

Visualisation
--------------------------

.. autofunction:: detectools.visualisation.visualisation

.. autofunction:: detectools.visualisation.draw_object

.. autofunction:: detectools.visualisation.draw_objects


Data management:

.. autofunction:: detectools.data_management.merge_jsons

.. autofunction:: detectools.data_management.split_dataset