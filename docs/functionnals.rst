Functionnals
===============

Here are functions that are used inside Predictor class and metrics.

Metrics
-----------

Here are all the functions that compose the processes above.

.. autofunction:: detectools.metrics.functionnals.match_boxes

.. autofunction:: detectools.metrics.functionnals.precision

.. autofunction:: detectools.metrics.functionnals.recall

.. autofunction:: detectools.metrics.functionnals.f1score

.. autofunction:: detectools.metrics.functionnals.iou

.. autofunction:: detectools.metrics.functionnals.accuracy

Inference
-----------

.. autofunction:: detectools.inference.engine.add_offset

.. autofunction:: detectools.inference.engine.remove_offset

.. autofunction:: detectools.inference.engine.pad_to

.. autofunction:: detectools.inference.engine.crop_to

.. autofunction:: detectools.inference.engine.patchification

.. autofunction:: detectools.inference.engine.unpatchification










