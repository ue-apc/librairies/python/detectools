Main Features
======================================

Loading data
------------

Detectools provides a custom Dataset (DetectionDataset) and a custom DataLoader (DetectionLoader) that inheriths from PyTorch Dataset & DataLoaders and allow to read & transform both images and targets.
To use this Dataset data should be stored in a folder that respect the following structure:

.. code-block:: text

    . Dataset Name
        ├── images
            ├── name01.png
            ├── name02.png
            └── ...
        └── coco_annotations.json

File "coco_annotations.json" contain detection annotations in `COCO format`_.

.. _`COCO format`:
    https://cocodataset.org/#format-data



.. autoclass:: detectools.data.DetectionDataset
    :members:


.. autoclass:: detectools.data.DetectionLoader
    :members:


Trainning process
-----------------

The whole trainning process can be done using Trainer class. It wraps all trainning utilities (optimizer, scheduler, tensorboard, etc.).

.. autoclass:: detectools.train.Trainer
    :members:

Inference process
-----------------

.. autoclass:: detectools.inference.Predictor
    :members:
