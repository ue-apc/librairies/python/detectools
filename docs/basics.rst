Basics
====================

The following objects are necessary to switch from detection mode (no mask) to instance segmentation.

.. autofunction:: detectools.set_lib_mode

.. autoclass:: detectools.Task
