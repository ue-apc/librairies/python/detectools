.. detectools documentation master file, created by
   sphinx-quickstart on Thu Apr 25 15:33:03 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to detectools's documentation!
======================================

Detectools in an overlay of PyTorch for object detection and instances segmentation tasks in images. It's development is supported by the `PHENOME - EMPHASIS`_ network.

.. _`PHENOME - EMPHASIS`:
   https://www.phenome-emphasis.fr/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   main_features
   basics
   usefull_features
   modules/models
   modules/formats
   modules/metrics
   modules/utils
   going_deeper
   functionnals
   tutorials/detection
   tutorials/instance_segmentation
   issues



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
