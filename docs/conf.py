# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import detectools
sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('../src'))



html_static_path = ['_static']


# Spéciale configuration pour Read the Docs






# -- Project information -----------------------------------------------------

project = 'detectools'
copyright = '2024, Lucas Bernigaud Samatan'
author = 'Lucas Bernigaud Samatan'

# The full version, including alpha/beta/rc tags
release = detectools.__version__

html_context = {
    "display_gitlab": True,
    "gitlab_host": "forgemia.inra.fr", # Integrate Gitlab
    "gitlab_user": "ue-apc",
    "gitlab_repo": "librairies/python/detectools", # Repo name
    "gitlab_version": "master", # Version
    "conf_py_path": "/docs/", # Path in the checkout to the docs root
}
if os.environ.get('READTHEDOCS'):
    html_context['commit'] = os.environ.get('READTHEDOCS_VERSION')


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx_rtd_theme',
    'nbsphinx',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.autosectionlabel'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

autodoc_inherit_docstrings = False
napoleon_google_docstring = True
napoleon_include_init_with_doc = True
napoleon_numpy_docstring = False

