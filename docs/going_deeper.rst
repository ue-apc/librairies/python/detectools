Going deeper
=============

To go deeper in the library here are some functions anc classes that aren't necessary to use when using classic trainning and inference pipeline but allow to develop custom processes.

.. autoclass:: detectools.inference.InferenceImage
    :members:

.. autoclass:: detectools.models.BaseModel
    :members:

.. autoclass:: detectools.formats.BaseAnnotation
    :members:

.. autoclass:: detectools.formats.BaseFormat
    :members:

.. autoclass:: detectools.metrics.base.DetectMetric
    :members:

.. autoclass:: detectools.metrics.base.ClassifMetric
    :members:

.. autoclass:: detectools.metrics.base.SemanticSegmentationMetric
    :members:

.. autoclass:: detectools.metrics.base.MetricCompose
    :members: