Metrics
======================================

Detectools provide metrics for detection, instance segmentation and semantic segmentation (semantic mask from a stack of object masks). Those metrics inherits from `Torchmetrics Metric class`_. ()

.. _`Torchmetrics Metric class`:
    https://lightning.ai/docs/torchmetrics/stable/references/metric.html#torchmetrics.Metric

.. automodule:: detectools.metrics
    :members: