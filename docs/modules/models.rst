Models
======================================

Detectools models inherits from abstract class detectools.BaseModel that define necessary functions to be use in trainning and inference process (through Trainer & Predictor classes).
They also inheriths from a class from the packages they come from (i.e. ultralytics or hugging face) to be used in development. 

.. automodule:: detectools.models
    :members: YoloDetection ,Yolov8Segmentation, Mask2Former