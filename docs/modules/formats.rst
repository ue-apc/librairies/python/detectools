Formats
======================================

Detectools provide a specific classes to wrap detection data (boxes, masks, labels, etc.) depending on the library task mode (detection or instance segmentation). These classes supports lots a basic transformations (crop, pad, indexation, ...) to manage targets and predictions data.

.. automodule:: detectools.formats
    :members: Format, Annotation, DetectionFormat, SegmentationFormat, DetectionAnnotation, SegmentationAnnotation, BatchedFormats
    :special-members: