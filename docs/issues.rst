Contribute
============

Issues are visible here_. To raise an issue or contribute you shall have a specific role on the project (guest or developer respectively). Please contact authors to ask for an access. 

.. _here:
    https://forgemia.inra.fr/ue-apc/librairies/python/detectools/-/issues



Known issues
=================

Patchification
---------------

* Patchification process leads to some false positive for objects that belong to multiple overlapping patches. The algortihm should be optimized and some parameters to remove duplicates should be implemented in future.

* Patchification with Mask2Former leads to important mistakes and will be investigated.
